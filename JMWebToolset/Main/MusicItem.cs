﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace JMWebToolset.Main
{
    public class MusicItem : JustItem
    {
        public override string filetype { get { return "nbs"; } }

        public string songName = "";
        public string songAuthor = "";
        public string originalAuthor = "";
        public string songDesc = "";
        public List<MusicBlock> noteblocks = new List<MusicBlock>();
        public List<string> customInstruments = new List<string>();
        public List<MusicItem> additionalItems = new List<MusicItem>();
        public MusicItem(bool fileСomplement = false) : base(fileСomplement)
        {
            noteblocks = new List<MusicBlock>();
        }

        public MusicItem(Stream stream, bool fileСomplement = false) : base(fileСomplement)
        {
            BinaryReader br = new BinaryReader(stream);

            bool IsNewFormat = br.ReadInt16() == 0;
            if (!IsNewFormat)
            {
                return;
            }

            br.ReadByte();
            br.ReadByte();
            br.ReadInt16();
            short layerCount = br.ReadInt16();

            string songName = readstring(br);
            string songAuthor = readstring(br);
            string originalAuthor = readstring(br);
            string songDesc = readstring(br);
            float songTempo = (float)br.ReadInt16() / 100f;

            br.ReadByte();
            br.ReadByte();
            br.ReadByte();
            br.ReadInt32();
            br.ReadInt32();
            br.ReadInt32();
            br.ReadInt32();
            br.ReadInt32();
            readstring(br);

            br.ReadByte();
            br.ReadByte();
            br.ReadInt16();

            double tick = 0;
            ushort jumps = 0;
            List<MusicBlock> noteblockList = new List<MusicBlock>();
            while (true)
            {
                jumps = br.ReadUInt16();
                if (jumps == 0)
                {
                    break;
                }
                tick += (double)jumps;
                noteblockList.Add(new PauseBlock(jumps));
                short layer = -1;
                while (true)
                {
                    jumps = br.ReadUInt16();
                    if (jumps == 0)
                    {
                        break;
                    }
                    layer += (short)jumps;
                    byte inst = br.ReadByte();
                    byte key = br.ReadByte();
                    byte vel = br.ReadByte();
                    byte pan = br.ReadByte();
                    short pitch = br.ReadInt16();
                    noteblockList.Add(new NoteBlock(inst, key, vel, pan, pitch, layer));
                }
            }

            List<Layer> layerList = new List<Layer>();

            for (int l = 0; l < layerCount; l++)
            {
                readstring(br);
                br.ReadByte();
                byte volume = br.ReadByte();
                byte stereo = br.ReadByte();
                layerList.Add(new Layer(volume, stereo));
            }

            for (int i = 0; i < noteblockList.Count; i++)
            {
                MusicBlock mb = noteblockList[i];
                if (mb != null)
                {
                    if (mb is NoteBlock)
                    {
                        NoteBlock nb = (NoteBlock)mb;
                        (noteblockList[i] as NoteBlock)!.Velocity = (byte)(int)((float)(nb.Velocity) * (float)(layerList[nb.Layer].volume) / 100f);
                    }
                }
            }

            int tempoChangerNum = -1;
            byte customInstrumentCount = br.ReadByte();
            for (int i = 0; i < customInstrumentCount; i++)
            {
                string ciName = readstring(br);
                string ciFile = readstring(br);
                byte ciPitch = br.ReadByte();
                byte ciPressKey = br.ReadByte();

                if (ciName == "Tempo Changer")
                    tempoChangerNum = customInstruments.Count;

                customInstruments.Add(ciName);

                Console.WriteLine("%1 %2 %3 %4"
                    .Replace("%1", ciName)
                    .Replace("%2", ciFile)
                    .Replace("%3", ciPitch.ToString())
                    .Replace("%4", ciPressKey.ToString()));
            }

            br.Dispose();
            stream.Dispose();

            tick = 0;
            for (int i = 0; i < noteblockList.Count; i++)
            {
                if (noteblockList[i] is NoteBlock)
                {
                    NoteBlock nb = (noteblockList[i] as NoteBlock)!;
                    if (nb.Instrument >= 16)
                    {
                        if (customInstruments[nb.Instrument - 16] == "Tempo Changer")
                        {
                            songTempo = (float)nb.realPitch / 15f;
                        }
                    }
                }
                else
                {
                    PauseBlock pb = (noteblockList[i] as PauseBlock)!;
                    tick += pb.Pause / songTempo * 20d;
                    if (tick > 1)
                    {
                        (noteblockList[i] as PauseBlock)!.Pause = (ushort)Math.Floor(tick);
                        tick = tick - Math.Floor(tick);
                    }
                    else
                    {
                        (noteblockList[i] as PauseBlock)!.Pause = 0;
                    }
                }
            }
            noteblockList.RemoveAll(s => (s is PauseBlock) && ((s as PauseBlock)!.Pause == 0));
            if (tempoChangerNum != -1)
            {
                noteblockList.RemoveAll(s => (s is NoteBlock) && ((s as NoteBlock)!.Instrument == tempoChangerNum + 16));
                customInstruments[tempoChangerNum] = "TC";
            }

            this.songName = songName;
            this.songAuthor = songAuthor;
            this.originalAuthor = originalAuthor;
            this.songDesc = songDesc;

            int maxCount = 10000;
            while (noteblockList.Count > maxCount)
            {
                List<MusicBlock> newNoteblockList = noteblockList.Skip(Math.Max(0, noteblockList.Count() - maxCount)).ToList();
                noteblockList.RemoveRange(noteblockList.Count - maxCount, maxCount);
                additionalItems.Add(new MusicItem(true)
                {
                    noteblocks = newNoteblockList,
                    customInstruments = customInstruments,
                });
            }
            additionalItems.Reverse();

            this.noteblocks = new List<MusicBlock>(noteblockList);
        }

        public void Play()
        {
            //Not implemented yet
        }

		public override string getFileHead()
        {
            HeadObject ho = new HeadObject()
            {
                customInstruments = customInstruments
            };
            return JsonSerializer.Serialize(ho).Replace("\"", "\\\"");
        }

		public override string getFileBody()
        {
            List<string> notesList = new List<string>();
            foreach (MusicBlock nb in noteblocks)
            {
                notesList.Add(nb.getString());
            }
            string notesString = String.Join("", notesList);
            string data = notesString;
            return data.Replace(" ", "").Replace("\t", "").Replace("\n", "").Replace("\r", "");
        }

        private static string readstring(BinaryReader br)
        {
            int len = br.ReadInt32();
            string str = "";
            for (int i = 0; i < len; i++) str += (char)br.ReadByte();
            return str;
        }

        private class Layer
        {
            public byte volume, stereo = 0;
            public Layer(Byte _volume, byte _stereo)
            {
                volume = _volume;
                stereo = _stereo;
            }
        }

        private class HeadObject
        {
            public List<string> customInstruments { get; set; }
            public HeadObject()
            {
                customInstruments = new List<string>();
            }
        }
    }
}
