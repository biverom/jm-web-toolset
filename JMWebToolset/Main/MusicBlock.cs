﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JMWebToolset.Main
{
    public abstract class MusicBlock
    {

        public MusicBlock()
        {

        }

        public ushort warp(ushort val)
        {
            val |= 0b1000_0000_0000_0000;
            ushort s = (ushort)(val & 0b11);
            val >>= 2;
            val |= (ushort)(s << 14);
            return val;

        }

        public abstract string getString();
    }
}

