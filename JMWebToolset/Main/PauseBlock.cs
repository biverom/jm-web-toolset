﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;

namespace JMWebToolset.Main
{
    public class PauseBlock : MusicBlock
    {
        public ushort Pause;

        public PauseBlock(ushort pause = 0) : base()
        {
            Pause = pause;

            //System.Diagnostics.Debug.WriteLine($"P: {Pause}");
            //System.Diagnostics.Debug.WriteLine($"P: {(Pause | 0b1000000000000000)}");
        }

        public override string getString()
        {
            return Encoding.Unicode.GetString(BitConverter.GetBytes(warp((ushort)(Pause | 0b0111_1110_0000_0000))));
            //return Convert.ToString(Pause, 2).PadLeft(8, '0');
        }
    }
}

