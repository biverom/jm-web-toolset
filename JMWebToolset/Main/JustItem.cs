﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JMWebToolset.Main
{
    public class JustItem
    {
        public string itemId => fileСomplement ? "minecraft:paper" : "minecraft:book";
        public string itemImage => "content/images/items/" + itemId.Replace(':', '/') + ".png";

		public bool fileСomplement = false;
        public virtual string filetype { get { return "none"; } }
        public JustItem(bool fileСomplement = false)
        {
            this.fileСomplement = fileСomplement;
        }

        public string id
        {
            get 
            {
                return """%itemtype%{PublicBukkitValues:{"justcreativeplus:filetype":"%filetype%","justcreativeplus:filehead":"%filehead%","justcreativeplus:filebody":"%filebody%"}}"""
                    .Replace("%itemtype%", itemId)
                    .Replace("%filetype%", filetype)
                    .Replace("%filehead%", fileHead)
                    .Replace("%filebody%", fileBody);
            }
        }

		public string fileHead => fileСomplement ? "complement" : getFileHead();
		public virtual string getFileHead()
        {
            return """{}""";
        }

		public string fileBody => getFileBody();
		public virtual string getFileBody()
        {
            return "";
        }
    }
}
