﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JMWebToolset.Main
{
    public class NoteBlock : MusicBlock
    {
        public short Pitch, Layer, realPitch;
        public byte Instrument, Key, Velocity, Pan;

        public NoteBlock(byte inst, byte key, byte velocity = 100, byte pan = 100, short pitch = 0, short layer = 0)
        {
            Instrument = (byte)(inst & 0b0011_1111);
            Key = (byte)(key & 0b0111_1111);
            Velocity = (byte)((velocity * 63 / 100) & 0b0011_1111);
            Pan = (byte)((pan * 31 / 200) & 0b0001_1111);
            Key += (byte)(pitch / 100);
            Pitch = (short)((((pitch % 100) + 100) * 63 / 200) & 0b0000_0000_0011_1111);
            Layer = layer;

            realPitch = pitch;
        }

        public override string getString()
        {
            uint dat = ((((((((((0) << 6) | (uint)Instrument) << 7) | (uint)Key) << 6) | (uint)Velocity) << 5) | (uint)Pan) << 6) | (uint)Pitch;

            return Encoding.Unicode.GetString(BitConverter.GetBytes((warp((ushort)(dat & 0x7fff)) << 16) | (warp((ushort)(dat >> 15)) & 0xffff)));
            //return Convert.ToString(dat, 2).PadLeft(16, '0');
        }
    }
}

